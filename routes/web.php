<?php

use App\Http\Controllers\DashboardController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\ShopController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [DashboardController::class, 'index'])->name('dashboard');

/* manage Shops */
Route::get('/shop/index', [ShopController::class, 'index'])->name('shop.index');
Route::get('/shop/create', [ShopController::class, 'create'])->name('shop.create');
Route::post('/shop/store', [ShopController::class, 'store'])->name('shop.store');
Route::get('/shop/edit/{id}', [ShopController::class, 'edit'])->name('shop.edit');
Route::post('/shop/update/{id}', [ShopController::class, 'update'])->name('shop.update');
Route::post('/shop/destroy', [ShopController::class, 'destroy'])->name('shop.destroy');

/* manage Products */
Route::get('/product/index', [ProductController::class, 'index'])->name('product.index');
Route::post('/product/get-table', [ProductController::class, 'getTable'])->name('product.get_table');
Route::get('/product/create', [ProductController::class, 'create'])->name('product.create');
Route::post('/product/store', [ProductController::class, 'store'])->name('product.store');
Route::get('/product/edit/{id}', [ProductController::class, 'edit'])->name('product.edit');
Route::post('/product/update/{id}', [ProductController::class, 'update'])->name('product.update');
Route::post('/product/destroy', [ProductController::class, 'destroy'])->name('product.destroy');

Route::post('/product/import', [ProductController::class, 'productImport'])->name('product.import');
Route::get('/product/export', [ProductController::class, 'productExport'])->name('product.export');