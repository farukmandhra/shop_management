<div class="main-sidebar sidebar-style-2">
    <aside id="sidebar-wrapper">
        <div class="sidebar-brand">
            <a href="index.html"> <span class="logo-name">Admin</span>
            </a>
        </div>
        <ul class="sidebar-menu">
            <li class="dropdown {{ Route::currentRouteName() == 'dashboard' ? 'active' : '' }}">
                <a href="{{ route('dashboard') }}" class="nav-link"><i class="fa-solid fa-chart-line pr-2"></i><span>Dashboard</span></a>
            </li>
            <li class="dropdown {{ Route::currentRouteName() == 'shop.index' ? 'active' : '' }}">
                <a href="{{ route('shop.index') }}" class="nav-link"><i class="fa-solid fa-cart-shopping pr-2"></i><span>Shops</span></a>
            </li>
            <li class="dropdown {{ Route::currentRouteName() == 'product.index' ? 'active' : '' }}">
                <a href="{{ route('product.index') }}" class="nav-link"><i class="fa-brands fa-product-hunt pr-2"></i><span>Products</span></a>
            </li>
        </ul>
    </aside>
</div>