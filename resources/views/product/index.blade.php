@extends('layouts.master')

@section('title', 'Product')

@section('css')

    <link rel="stylesheet" href="{{ asset('bundles/sweetalert2/sweetalert2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/app.min.css') }}">
    <link rel="stylesheet" href="{{ asset('bundles/datatables/datatables.min.css') }}">
    <link rel="stylesheet" href="{{ asset('bundles/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css') }}">

@endsection

@section('content')

    <div class="main-content">
        <section class="section">
            <div class="section-body">

                <div class="row">
                    <div class="col-12">
                        <div class="card p-3">
                            <div class="d-flex justify-content-between">
                                <div>
                                    <form action="{{ route('product.import') }}" method="POST" enctype="multipart/form-data">
                                        @csrf
                                        <div class="form-group d-inline-block">
                                            <div></div>
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input" id="products_file" name="products_file" required=""/>
                                                <label class="custom-file-label" for="products_file">Choose file</label>
                                            </div>
                                        </div>
                                        <div class="form-group d-inline-block mr-2">
                                            <button class="btn btn-success">Import Products</button>
                                        </div>
                                        <div>
                                            <a class="" href="{{ asset('sample/products.csv') }}" download="">Download sample</a>
                                        </div>
                                    </form>
                                </div>
                                <div>
                                    <a href="{{ route('product.export') }}" class="btn btn-success">Export products</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header justify-content-between">
                                <h4>Products</h4>
                                <div class="col-md-3">
                                    <input type="number" class="form-control" id="min-price" placeholder="Min price">
                                </div>
                                <div class="col-md-3">
                                    <input type="number" class="form-control" id="max-price" placeholder="Max price">
                                </div>
                                <div class="col-md-3">
                                    <select class="form-control" id="stock">
                                        <option value="" selected>All</option>
                                        <option value="1">In-stock</option>
                                        <option value="0">out-stock</option>
                                    </select>
                                </div>
                                <a href="{{ route('product.create') }}" class="btn btn-primary">Add</a>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive" id="products-table-container">
                                
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

@endsection

@section('js')

    <script src="{{ asset('bundles/datatables/datatables.min.js') }}"></script>
    <script src="{{ asset('bundles/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('bundles/jquery-ui/jquery-ui.min.js') }}"></script>

    <script src="{{ asset('js/page/datatables.js') }}"></script>
    <script src="{{ asset('bundles/sweetalert2/sweetalert2.min.js') }}"></script>

    <script>

        $(".deleteBtn").click(function() {

            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.isConfirmed) {

                    var id = $(this).data("id");
                    var token = '{{ csrf_token() }}';

                    $.ajax({
                        url: '{{ route("product.destroy") }}',
                        method: "POST",
                        data: {
                            _token: token,
                            id: id
                        },
                        success: function() {

                            Swal.fire({
                                title: 'Deleted!',
                                text: 'User has been deleted.',
                                icon: 'success',
                            }).then((data) => {
                                location.reload();
                            });
                        }
                    });
                }
            });
        });

        $(document).ready(function() {
            getProductsTable();
        });

        $('#min-price').change(function() {
            getProductsTable();
        });
        $('#max-price').change(function() {
            getProductsTable();
        });
        $('#stock').change(function() {
            getProductsTable();
        });

        function getProductsTable() {

            var min_price = $('#min-price').val();
            var max_price = $('#max-price').val();
            var stock = $('#stock').val();

            $.ajax({
                url: '{{ route("product.get_table") }}',
                type: "POST",
                data: {
                    _token: '{{ csrf_token() }}',
                    min_price: min_price,
                    max_price: max_price,
                    stock: stock
                },
                success: function(result) {

                    $('#products-table-container').html(result);
                    $('#products-table').DataTable();
                }
            });
        }

    </script>

@endsection
