<table class="table table-striped table-hover" id="products-table" style="width:100%;">
    <thead>
        <tr>
            <th>No.</th>
            <th>Image</th>
            <th>Shop</th>
            <th>Name</th>
            <th>Price</th>
            <th>Stock</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        @if(isset($products) && !empty($products))
            @foreach($products as $key => $product)
                <tr>
                    <td>{{ $key + 1 }}</td>
                    <td> <img src="{{ asset('storage/'.$product->image) }}" width="50" height="50" alt=""> </td>
                    <td>{{ isset($product->shop->name) ? $product->shop->name : '' }}</td>
                    <td>{{ $product->name }}</td>
                    <td>{{ $product->price }}</td>
                    <td>{{ $product->stock }}</td>
                    <td>
                        <a class="btn btn-danger deleteBtn" href="javascript:;" id="deleteBtn" data-id="{{ $product->id }}"> <i class="fa fa-trash"></i> </a>
                        <a class="btn btn-default" href="{{ route('product.edit', $product->id) }}"> <i class="fa fa-pen"></i> </a>
                    </td>
                </tr>
            @endforeach
        @endif
    </tbody>
</table>