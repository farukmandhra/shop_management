@extends('layouts.master')

@section('title', 'Product')

@section('css')
    <link rel="stylesheet" href="{{ asset('bundles/select2/dist/css/select2.min.css') }}">
@endsection

@section('content')

    <div class="main-content">
        <section class="section">
            <div class="section-body">

                <div class="card">
                    <div class="card-header">
                        <h4>Add product</h4>
                    </div>
                    <form method="POST" action="{{ route('product.store') }}" enctype="multipart/form-data">
                        @csrf
                        <div class="card-body">
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="Shop">Shop</label>
                                    <select class="form-control select2" name="shop_id" id="shop_id">
                                        <option value="">Select Shop</option>
                                        @if(isset($shops) && !empty($shops))
                                            @foreach($shops as $id => $shopName)
                                                <option value="{{ $id }}">{{ $shopName }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                    @if($errors->has('shop_id'))
                                        <div class="text-danger">{{ $errors->first('shop_id') }}</div>
                                    @endif
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="name">Name</label>
                                    <input type="text" class="form-control" id="name" name="name" placeholder="Name">
                                    @if($errors->has('name'))
                                        <div class="text-danger">{{ $errors->first('name') }}</div>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="price">Price</label>
                                <input type="text" class="form-control" id="price" name="price" placeholder="Price">
                                @if($errors->has('price'))
                                    <div class="text-danger">{{ $errors->first('price') }}</div>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="stock">Stock</label>
                                <input type="text" class="form-control" id="stock" name="stock" placeholder="Stock">
                            </div>
                            <div class="form-group">
                                <label for="image-preview">Image</label>
                                <div id="image-preview" class="image-preview">
                                    <label for="image-upload" id="image-label">Choose File</label>
                                    <input type="file" name="image" id="image-upload" />
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <button class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </section>
    </div>

@endsection

@section('js')
<script src="{{ asset('bundles/upload-preview/assets/js/jquery.uploadPreview.min.js') }}"></script>
<script src="{{ asset('bundles/select2/dist/js/select2.full.min.js') }}"></script>
<script>

    $('.select2').select2();

    $.uploadPreview({
        input_field: "#image-upload",   // Default: .image-upload
        preview_box: "#image-preview",  // Default: .image-preview
        label_field: "#image-label",    // Default: .image-label
        label_default: "Choose File",   // Default: Choose File
        label_selected: "Change File",  // Default: Change File
        no_label: false,                // Default: false
        success_callback: null          // Default: null
    });
</script>
@endsection
