@extends('layouts.master')

@section('title', 'Shop')

@section('css')

    <link rel="stylesheet" href="{{ asset('bundles/sweetalert2/sweetalert2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/app.min.css') }}">
    <link rel="stylesheet" href="{{ asset('bundles/datatables/datatables.min.css') }}">
    <link rel="stylesheet" href="{{ asset('bundles/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css') }}">

@endsection

@section('content')

    <div class="main-content">
        <section class="section">
            <div class="section-body">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                        <div class="card-header justify-content-between">
                            <h4>Shops</h4>
                            <a href="{{ route('shop.create') }}" class="btn btn-primary">Add</a>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                            <table class="table table-striped table-hover" id="save-stage" style="width:100%;">
                                <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Image</th>
                                        <th>Email</th>
                                        <th>Name</th>
                                        <th>Address</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(isset($shops) && !empty($shops))
                                        @foreach($shops as $key => $shop)
                                            <tr>
                                                <td>{{ $key + 1 }}</td>
                                                <td> <img src="{{ asset('storage/'.$shop->image) }}" width="50" height="50" alt=""> </td>
                                                <td>{{ $shop->email }}</td>
                                                <td>{{ $shop->name }}</td>
                                                <td>{{ $shop->address }}</td>
                                                <td>
                                                    <a class="btn btn-danger deleteBtn" href="javascript:;" id="deleteBtn" data-id="{{ $shop->id }}"> <i class="fa fa-trash"></i> </a>
                                                    <a class="btn btn-default" href="{{ route('shop.edit', $shop->id) }}"> <i class="fa fa-pen"></i> </a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

@endsection

@section('js')

    <script src="{{ asset('bundles/datatables/datatables.min.js') }}"></script>
    <script src="{{ asset('bundles/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('bundles/jquery-ui/jquery-ui.min.js') }}"></script>

    <script src="{{ asset('js/page/datatables.js') }}"></script>
    <script src="{{ asset('bundles/sweetalert2/sweetalert2.min.js') }}"></script>

    <script>

        $(".deleteBtn").click(function() {

            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.isConfirmed) {

                    var id = $(this).data("id");
                    var token = '{{ csrf_token() }}';

                    $.ajax({
                        url: '{{ route("shop.destroy") }}',
                        method: "POST",
                        data: {
                            _token: token,
                            id: id
                        },
                        success: function() {

                            Swal.fire({
                                title: 'Deleted!',
                                text: 'User has been deleted.',
                                icon: 'success',
                            }).then((data) => {
                                location.reload();
                            });
                        }
                    });
                }
            });
        });

    </script>

@endsection
