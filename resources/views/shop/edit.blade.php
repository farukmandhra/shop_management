@extends('layouts.master')

@section('title', 'Shop')

@section('css')

@endsection

@section('content')

    <div class="main-content">
        <section class="section">
            <div class="section-body">
                <div class="card">
                    <div class="card-header">
                        <h4>Edit shop</h4>
                    </div>
                    <form method="POST" action="{{ route('shop.update', $shop->id) }}" enctype="multipart/form-data">
                        @csrf
                        <div class="card-body">
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" class="form-control" id="name" name="name" value="{{ $shop->name }}" placeholder="Shop Name">
                                @if($errors->has('name'))
                                    <div class="text-danger">{{ $errors->first('name') }}</div>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="address">Address</label>
                                <input type="text" class="form-control" id="address" name="address" value="{{ $shop->address }}" placeholder="Shop address">
                            </div>
                            <div class="form-group">
                                <label for="image-preview">Image</label>
                                <div id="image-preview" class="image-preview" style="background-image: url('{{ asset('storage/'.$shop->image) }}' ); background-size: cover; background-position: center center;">
                                    <label for="image-upload" id="image-label">Change File</label>
                                    <input type="file" name="new_image" id="image-upload">
                                </div>
                            </div>
                            <input type="hidden" name="image" value="{{ $shop->image }}">
                        </div>
                        <div class="card-footer">
                            <button class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </section>
    </div>

@endsection

@section('js')
<script src="{{ asset('bundles/upload-preview/assets/js/jquery.uploadPreview.min.js') }}"></script>
<script>
    $.uploadPreview({
        input_field: "#image-upload",   // Default: .image-upload
        preview_box: "#image-preview",  // Default: .image-preview
        label_field: "#image-label",    // Default: .image-label
        label_default: "Choose File",   // Default: Choose File
        label_selected: "Change File",  // Default: Change File
        no_label: false,                // Default: false
        success_callback: null          // Default: null
    });
</script>
@endsection
