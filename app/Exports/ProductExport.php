<?php

namespace App\Exports;

use App\Models\Product;
use App\Models\Shop;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMapping;

class ProductExport implements FromCollection, WithMapping, WithHeadings
{
    use Exportable;
    private $product;

    public function __construct(Product $product)
    {
        $this->product = $product;
    }
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Product::with('shop')->select('shop_id', 'name', 'price', 'stock')->get();
    }

    public function map($prod): array
    {
        return [
            isset($prod->shop->name) ? $prod->shop->name : '',
            $prod->name,
            $prod->price,
            $prod->stock,
        ];
    }

    public function headings(): array
    {
        return ["shop", "name", "price", "stock"];
    }
}
