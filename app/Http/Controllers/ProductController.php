<?php

namespace App\Http\Controllers;

use App\Exports\ProductExport;
use App\Models\Product;
use App\Models\Shop;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use App\Imports\ProductsImport;
use App\Exports\UsersExport;
use Maatwebsite\Excel\Facades\Excel;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {

        return view('product.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request) {

        $shops = Shop::all()->pluck('name', 'id')->toArray();
        $return['shops'] = $shops;

        return view('product.add', $return);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {

        $request->validate([
            'shop_id' => 'required',
            'name' => 'required|unique:products,name,NULL,id,shop_id,'.$request->shop_id,
            'price' => 'required'
        ], [
            'shop_id.required' => 'Please select Shop!',
            'name.required' => 'Please enter Name.',
            'name.unique' => 'Product with same name already exists.',
            'price.required' => 'Please enter Price.'
        ]);

        $image_path = "";
        if ($request->hasFile('image')) {
            $image      = $request->file('image');
            $image_path = $image->store('product', ['disk' => 'public']);
        }

        $shop = new Product();
        $shop->shop_id = $request->shop_id;
        $shop->image = $image_path;
        $shop->name = $request->name;
        $shop->price  = $request->price;
        $shop->stock  = $request->stock;
        $shop->save();

        if ($shop) {

            return redirect()->route('product.index')->with("success", "Details has been saved successfully.");
        } else {
            return redirect()->back()->with("error", "Details not saved, please try again!");
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::find($id);
        $return['product'] = $product;

        $shops = Shop::all()->pluck('name', 'id')->toArray();
        $return['shops'] = $shops;

        return view('product.edit', $return);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $product = Product::where('id', $id)->first();

        $request->validate([
            'name' => 'required|unique:products,name,'.$id,
            'price' => 'required',
            'stock' => 'required'
        ], [
            'name.required' => 'Please enter Name.',
            'price.required' => 'Please enter Price.',
            'stock.required' => 'Please enter Stock.',
        ]);

        $image_path = "";
        if ($request->hasFile('new_image')) {
            $image      = $request->file('new_image');
            $image_path = $image->store('product', ['disk' => 'public']);

            if($request->image != '') {
                $oldFilename = public_path('storage/'.$request->image);
                if (file_exists($oldFilename)) {
                    unlink($oldFilename);
                }
            }
        }

        $product->image = $image_path;
        $product->name = $request->name;
        $product->price  = $request->price;
        $product->stock  = $request->stock;
        $product->save();

        if ($product) {
            return redirect()->route('product.index')->with("success", "Details has been updated successfully.");
        } else {
            return redirect()->back()->with("error", "Details not saved, please try again!");
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $id = $request->id;

        $product = Product::where('id', $id)->first();

        $oldFilename = public_path('storage/'.$product->image);
        if (file_exists($oldFilename)) {
            unlink($oldFilename);
        }

        $product = Product::where('id', $id)->delete();

        $return['status'] = 'success';
        return response()->json($return);
    }

    public function getTable(Request $request) {

        $minPrice = $request->min_price;
        $maxPrice = $request->max_price;
        $stock = $request->stock;

        $productsQuery = Product::orderBy('id');

        if($minPrice > 0 || $maxPrice > 0) {
            $productsQuery->whereBetween('price', [$minPrice, $maxPrice]);
        }
        if(isset($stock) && $stock == 1) {
            $productsQuery->where('stock', '>', 0);
        }
        if(isset($stock) && $stock == 0) {
            $productsQuery->where('stock', 0);
        }
        $products = $productsQuery->get();

        $return['products'] = $products;

        return View::make('product.table', $return);
    }

    public function productImport() 
    {
        Excel::import(new ProductsImport,request()->file('products_file'));

        return back();
    }

    public function productExport(Product $product) 
    {
        return Excel::download(new ProductExport($product), 'products.csv');
    }
}
