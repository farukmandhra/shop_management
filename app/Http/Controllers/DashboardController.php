<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\Shop;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index() {

        $shopCount = Shop::count();
        $return['shopCount'] = $shopCount;

        $productCount = Product::count();
        $return['productCount'] = $productCount;
        
        return view('dashboard', $return);
    }
}
