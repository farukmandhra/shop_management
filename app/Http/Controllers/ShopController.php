<?php

namespace App\Http\Controllers;

use App\Models\Shop;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class ShopController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {

        $shops = Shop::all();
        $return['shops'] = $shops;

        return view('shop.index', $return);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request) {

        return view('shop.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {

        $request->validate([
            'name' => 'required',
            'email' => 'required|email|unique:shops',
            'password' => 'required'
        ], [
            'email.required' => 'Please enter Email Address!',
            'email.email' => 'Please enter valid Email Address!',
            'name.required' => 'Please enter Name.',
            'password.required' => 'Please enter Password.'
        ]);

        $image_path = "";
        if ($request->hasFile('image')) {
            $image      = $request->file('image');
            $image_path = $image->store('shop', ['disk' => 'public']);
        }

        $shop = new Shop();
        $shop->image = $image_path;
        $shop->name = $request->name;
        $shop->email  = $request->email;
        $shop->password  = Hash::make($request->password);
        $shop->address  = $request->address;
        $shop->save();

        if ($shop) {

            return redirect()->route('shop.index')->with("success", "Details has been saved successfully.");
        } else {
            return redirect()->back()->with("error", "Details not saved, please try again!");
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $shop = Shop::find($id);
        $return['shop'] = $shop;

        return view('shop.edit', $return);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $shop = Shop::where('id', $id)->first();

        $request->validate([
            'name' => 'required',
        ], [
            'name.required' => 'Please enter Name.',
        ]);

        $image_path = "";
        if ($request->hasFile('new_image')) {
            $image      = $request->file('new_image');
            $image_path = $image->store('shop', ['disk' => 'public']);

            $oldFilename = public_path('storage/'.$request->image);
            if (file_exists($oldFilename)) {
                unlink($oldFilename);
            }
        }

        $shop->image = $image_path;
        $shop->name = $request->name;
        $shop->address  = $request->address;
        $shop->save();

        if ($shop) {
            return redirect()->route('shop.index')->with("success", "Details has been updated successfully.");
        } else {
            return redirect()->back()->with("error", "Details not saved, please try again!");
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $id = $request->id;

        $shop = Shop::where('id', $id)->first();

        $oldFilename = public_path('storage/'.$shop->image);
        if (file_exists($oldFilename)) {
            unlink($oldFilename);
        }

        $shop = Shop::where('id', $id)->delete();

        $return['status'] = 'success';
        return response()->json($return);
    }
}
