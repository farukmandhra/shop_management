<?php

namespace App\Imports;

use App\Models\Product;
use App\Models\Shop;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class ProductsImport implements ToModel, WithHeadingRow
{
    private $shops;

    public function __construct() {

        $this->shops = Shop::select('id', 'name')->get();
    }

    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        $shop = $this->shops->where('name', $row['shop'])->first();

        return new Product([
            'shop_id' => isset($shop->id) && $shop->id != NULL ? $shop->id : NULL,
            'name' => $row['name'],
            'price' => $row['price'],
            'stock' => $row['stock'],
        ]);
    }
}
