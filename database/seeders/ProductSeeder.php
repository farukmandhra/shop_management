<?php

namespace Database\Seeders;

use App\Models\Product;
use App\Models\Shop;
use Faker\Generator as Faker;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Faker $faker)
    {
        $shops = collect(Shop::all()->modelKeys());
        $data = [];

        for ($i = 0; $i < 100; $i++) {
            $data[] = [
                'shop_id' => $shops->random(),
                'name' => $faker->company,
                'price' => $faker->randomNumber(3),
                'stock' => $faker->randomNumber(2),
                'created_at' => now()->toDateTimeString(),
                'updated_at' => now()->toDateTimeString(),
            ];
        }

        $chunks = array_chunk($data, 100);

        foreach ($chunks as $chunk) {
            Product::insert($chunk);
        }
    }
}
